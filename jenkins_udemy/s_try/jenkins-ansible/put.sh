# this script is for fill the db people in mysql inside the container for
# ansible udemy course jorgescalona 2021
#!/bin/bash

counter=0

while [ $counter -lt 50 ]; do
    (( counter=counter+1 ))
    echo "CONTADOR = $counter"
    name=$(nl people.txt | grep -w $counter | awk '{print $2}' | awk -F ',' '{print $1}')
    apellido=$(nl people.txt | grep -w $counter | awk '{print $2}' | awk -F ',' '{print $2}')
    edad=$(shuf -i 20-25 -n 1)

    mysql -u root -p1234 people -e "INSERT INTO registro VALUES ($counter, '$name', '$apellido', $edad)"
    echo "**** $counter, $name, $apellido, $edad, se importaron correctamente ****"
done
