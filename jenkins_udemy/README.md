# Jenkins Master from Udemy

![jekins](static/images/jenkins_ico.jpg)

This repo is a collection of personal notes about the learning process of jenkins
through udemy platform

## Course resources

* [repo github](https://github.com/ricardoandre97/jenkins-resources)

## Interesting links:

* [Add private repository gitlab or bitbucket 2 jenkins](https://www.youtube.com/watch?v=HTlAssPBKBs "Add private repo")
* [Integrate gitlab hooks with jenkins](https://www.youtube.com/watch?v=65hrCvYMLqo "GitlabHookJenkins")
* [job-dsl plugin](https://plugins.jenkins.io/job-dsl/ "plugin for configuration as code")
* [groovy syntax apache](https://groovy-lang.org/syntax.html "groovy syntax apache")
* [groovy syntax jenkins](https://www.jenkins.io/doc/book/pipeline/syntax/ "groovy syntax jenkins")
* [How to setup ssh keys by DO](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-2 "setup ssh-keys")

@jorgemustaine 2020/2021
