# Vertex AI: Predicting Loan Risk with AutoML

In this lab, you use [Vertex AI](https://cloud.google.com/vertex-ai/docs) to train and serve a machine learning model to predict loan risk with a tabular dataset.

Vertex AI offers two options on one platform to build a ML model: a codeless solution with AutoML and a code-based solution with Custom Training using Vertex Workbench. You use AutoML in this lab.

In this lab you build a ML model to determine whether a particular customer will repay a loan.

### Upload data

Three options to import data in Vertex AI:

* Upload a local file from your computer.
* Select files from Cloud Storage.
* Select data from BigQuery.


Veretex AI provides many metrics to evaluate the model performance. You focus on three:

* Precision/Recall curve
* Confusion Matrix
* Feature Importance



### interesting links:

[LabVideo](https://www.youtube.com/watch?v=LF6MsJ8dUek)
