# Running Apache Spark jobs on Cloud Dataproc

In this lab you will learn how to migrate Apache Spark code to Cloud Dataproc. You will follow a sequence of steps progressively moving more of the job components over to GCP services:

* Run original Spark code on Cloud Dataproc (Lift and Shift)

* Replace HDFS with Cloud Storage (cloud-native)

* Automate everything so it runs on job-specific clusters (cloud-optimized)

### Scenario

You are migrating an existing Spark workload to Cloud Dataproc and then progressively modifying the Spark code to make use of GCP native features and services.

### Task.1

**Migrate existing Spark jobs to Cloud Dataproc**

You will create a new Cloud Dataproc cluster and then run an imported Jupyter notebook that uses the cluster's default local Hadoop Distributed File system (HDFS) to store source data and then process that data just as you would on any Hadoop cluster using Spark. This demonstrates how many existing analytics workloads such as Jupyter notebooks containing Spark code require no changes when they are migrated to a Cloud Dataproc environment.

**Configure and start a Cloud Dataproc cluster**


In the GCP Console, on the Navigation menu, in the Analytics section, click Dataproc.

1. Click Create Cluster.

1. Click Create for the item Cluster on Compute Engine.

   Enter sparktodp for Cluster Name.

1. In the Versioning section, click Change and select 2.0 (Debian 10, Hadoop 3.2, Spark 3.1).

    This version includes Python3, which is required for the sample code used in this lab.

1. Click Select.

1. In the Components > Component gateway section, select Enable component gateway.

1. Under Optional components, Select Jupyter Notebook.

1. Click Create.

The cluster should start in a few minutes. You can proceed to the next step without waiting for the Cloud Dataproc Cluster to fully deploy.

**Clone the source repository for the lab**

In the Cloud Shell you clone the Git repository for the lab and copy the required notebook files to the Cloud Storage bucket used by Cloud Dataproc as the home directory for Jupyter notebooks.

1. To clone the Git repository for the lab enter the following command in Cloud Shell:

`git -C ~ clone https://github.com/GoogleCloudPlatform/training-data-analyst`

2. To locate the default Cloud Storage bucket used by Cloud Dataproc enter the following command in Cloud Shell:

```
export DP_STORAGE="gs://$(gcloud dataproc clusters describe sparktodp --region=us-central1 --format=json | jq -r '.config.configBucket')"
```

3. To copy the sample notebooks into the Jupyter working folder enter the following command in Cloud Shell:

`gsutil -m cp ~/training-data-analyst/quests/sparktobq/*.ipynb $DP_STORAGE/notebooks/jupyter`

### Log in to the Jupyter Notebook

1. On the Dataproc Clusters page wait for the cluster to finish starting and then click the name of your cluster to open the Cluster details page.

1. Click Web Interfaces.

1. Click the Jupyter link to open a new Jupyter tab in your browser.

This opens the Jupyter home page. Here you can see the contents of the `/notebooks/jupyter` directory in Cloud Storage that now includes the sample Jupyter notebooks used in this lab.

4. Under the Files tab, click the GCS folder and then click 01_spark.ipynb notebook to open it.

1. Click Cell and then Run All to run all of the cells in the notebook.

1. Page back up to the top of the notebook and follow as the notebook completes runs each cell and outputs the results below them.

You can now step down through the cells and examine the code as it is processed so that you can see what the notebook is doing. In particular pay attention to where the data is saved and processed from.

### Task 2. Separate compute and storage

**Modify Spark jobs to use Cloud Storage instead of HDFS**

Taking this original 'Lift & Shift' sample notebook you will now create a copy that decouples the storage requirements for the job from the compute requirements. In this case, all you have to do is replace the Hadoop file system calls with Cloud Storage calls by replacing `hdfs://` storage references with `gs://` references in the code and adjusting folder names as necessary.

You start by using the cloud shell to place a copy of the source data in a new Cloud Storage bucket.

1. In the Cloud Shell create a new storage bucket for your source data:

```
export PROJECT_ID=$(gcloud info --format='value(config.project)')
gsutil mb gs://$PROJECT_ID
```

2. In the Cloud Shell copy the source data into the bucket:

```
wget https://archive.ics.uci.edu/ml/machine-learning-databases/kddcup99-mld/kddcup.data_10_percent.gz
gsutil cp kddcup.data_10_percent.gz gs://$PROJECT_ID/
```


