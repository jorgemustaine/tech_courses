# Serverless Data Processing with Dataflow

Cloud Dataflow is a fully-managed Google Cloud Platform service for running batch and streaming Apache Beam data processing pipelines.

Apache Beam is an open source, advanced, unified and portable data processing programming model that allows end users to define both batch and streaming data-parallel processing pipelines using Java, Python, or Go. Apache Beam pipelines can be executed on your local development machine on small datasets, and at scale on Cloud Dataflow. However, because Apache Beam is open source, other runners exist — you can run Beam pipelines on Apache Flink and Apache Spark, among others.

![dataflow structure](../static/images/df_lab_00.png "dataflow structure")

### Lab part 1: Writing an ETL pipeline from scratch


**Introduction:**


In this section, you write an Apache Beam Extract-Transform-Load (ETL) pipeline from scratch.

## Dataset and use case review

For each lab in this quest, the input data is intended to resemble web server logs in [Common Log format](https://en.wikipedia.org/wiki/Common_Log_Format) along with other data that a web server might contain. For this first lab, the data is treated as a batch source; in later labs, the data will be treated as a streaming source. Your task is to read the data, parse it, and then write it to BigQuery, a serverless data warehouse, for later data analysis..

