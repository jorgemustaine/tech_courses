# Working with JSON and Array data in BigQuery 2.5


[BigQuery](https://cloud.google.com/bigquery "gcp bigquery") is Google's fully managed, NoOps, low cost analytics database. With BigQuery you can query terabytes and terabytes of data without having any infrastructure to manage or needing a database administrator. BigQuery uses SQL and can take advantage of the pay-as-you-go model. BigQuery allows you to focus on analyzing data to find meaningful insights.

### Practice working with Arrays in SQL


Normally in SQL you will have a single value for each row like this list of fruits below:


| Row | Fruit |
|-|-|
| 1 | raspberry |
| 2 | blackberry |
| 3 | strawberry
| 4 | cherry

What if you wanted a list of fruit items for each person at the store? It could look something like this:

| Row | Fruit | Person |
|-|-|-|
| 1 | raspberry | sally |
| 2|blackberry|sally
|3|strawberry|sally
|4|cherry|sally
|5|orange|frederick
|6|apple|frederick

In traditional relational database SQL, you would look at the repetition of names and immediately think to split the above table into two separate tables: Fruit Items and People. That process is called normalization (going from one table to many). This is a common approach for transactional databases like mySQL.

For data warehousing, data analysts often go the reverse direction (denormalization) and bring many separate tables into one large reporting table.

![Structs](../static/images/bq_structs_and_arrays_00.png "Structs and arrays in bigquery")

### Creating your own arrays with ARRAY_AGG()

Don't have arrays in your tables already? You can create them!

explore this public dataset:

```
SELECT
  fullVisitorId,
  date,
  v2ProductName,
  pageTitle
  FROM `data-to-insights.ecommerce.all_sessions`
WHERE visitId = 1501570398
ORDER BY date
```

Now, we will use the ARRAY_AGG() function to aggregate our string values into an array. Copy and paste the below query to explore this public dataset:

```
SELECT
  fullVisitorId,
  date,
  ARRAY_AGG(v2ProductName) AS products_viewed,
  ARRAY_AGG(pageTitle) AS pages_viewed
  FROM `data-to-insights.ecommerce.all_sessions`
WHERE visitId = 1501570398
GROUP BY fullVisitorId, date
ORDER BY date
```

Next, we will use the ARRAY_LENGTH() function to count the number of pages and products that were viewed.

```
SELECT
  fullVisitorId,
  date,
  ARRAY_AGG(v2ProductName) AS products_viewed,
  ARRAY_LENGTH(ARRAY_AGG(v2ProductName)) AS num_products_viewed,
  ARRAY_AGG(pageTitle) AS pages_viewed,
  ARRAY_LENGTH(ARRAY_AGG(pageTitle)) AS num_pages_viewed
  FROM `data-to-insights.ecommerce.all_sessions`
WHERE visitId = 1501570398
GROUP BY fullVisitorId, date
ORDER BY date
```

Next, lets deduplicate the pages and products so we can see how many unique products were viewed. We'll simply add DISTINCT to our ARRAY_AGG()

```
SELECT
  fullVisitorId,
  date,
  ARRAY_AGG(DISTINCT v2ProductName) AS products_viewed,
  ARRAY_LENGTH(ARRAY_AGG(DISTINCT v2ProductName)) AS distinct_products_viewed,
  ARRAY_AGG(DISTINCT pageTitle) AS pages_viewed,
  ARRAY_LENGTH(ARRAY_AGG(DISTINCT pageTitle)) AS distinct_pages_viewed
  FROM `data-to-insights.ecommerce.all_sessions`
WHERE visitId = 1501570398
GROUP BY fullVisitorId, date
ORDER BY date
```

*Recap:*

You can do some pretty useful things with arrays like:

* finding the number of elements with ARRAY_LENGTH(<array>)
* deduplicating elements with ARRAY_AGG(DISTINCT <field>)
* ordering elements with ARRAY_AGG(<field> ORDER BY <field>)
* limiting ARRAY_AGG(<field> LIMIT 5)

### Querying datasets that already have ARRAYs

The BigQuery Public Dataset for Google Analytics `bigquery-public-data.google_analytics_sample` has many more fields and rows than our course dataset `data-to-insights.ecommerce.all_sessions`. More importantly, it already stores field values like products, pages, and transactions natively as ARRAYs.

Copy and Paste the below query to explore the available data and see if you can find fields with repeated values (arrays):

```
SELECT
  *
FROM `bigquery-public-data.google_analytics_sample.ga_sessions_20170801`
WHERE visitId = 1501570398
```

The amount of fields available in the Google Analytics schema can be overwhelming for our analysis. Let's try to query just the visit and page name fields like we did before.

```
SELECT
  visitId,
  hits.page.pageTitle
FROM `bigquery-public-data.google_analytics_sample.ga_sessions_20170801`
WHERE visitId = 1501570398
```

### Introduction to STRUCTs

You may have wondered why the field alias `hit.page.pageTitle` looks like three fields in one separated by periods. Just as ARRAY values give you the flexibility to go deep into the granularity of your fields, another data type allows you to go wide in your schema by grouping related fields together. That SQL data type is the [STRUCT](https://cloud.google.com/bigquery/docs/reference/standard-sql/data-types#struct-type "struct official documentation") data type.

The easiest way to think about a STRUCT is to consider it conceptually like a separate table that is already pre-joined into your main table.

A STRUCT can have:

* one or many fields in it
* the same or different data types for each field
* it's own alias


Sounds just like a table right?

*To recap:*

* Structs are containers that can have multiple field names and data types nested inside.

* An arrays can be one of the field types inside of a Struct (as shown above with the splits field).


Example from edit schema:

```
[
    {
        "name": "race",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "participants",
        "type": "RECORD",
        "mode": "REPEATED",
        "fields": [
            {
                "name": "name",
                "type": "STRING",
                "mode": "NULLABLE"
            },
            {
                "name": "splits",
                "type": "FLOAT",
                "mode": "REPEATED"
            }
        ]
    }
]
```
