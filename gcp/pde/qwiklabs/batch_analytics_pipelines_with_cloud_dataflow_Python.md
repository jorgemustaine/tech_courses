# Batch Analytics Pipelines with Cloud Dataflow (Python)

### **Overview**

In this lab, you:

* Write a pipeline that aggregates site traffic by user.
* Write a pipeline that aggregates site traffic by minute.
* Implement windowing on time series data.


### Prepare the IDE environment

1. Go to VertexAI service and select Workbench, make enable Notebooks API.
1. At the top of the page click New Notebook, and select Theia IDE [Experimental] > Without GPUs.
1. In the dialog box that appears, set the region to us-central1 and then click CREATE at the bottom.

### Download Code Repository

```
git clone https://github.com/GoogleCloudPlatform/training-data-analyst
cd ~/project/training-data-analyst/quests/dataflow_python/
```
1. Execute the following to create a virtual environment for your work in this lab:

```
# Change directory into the lab
cd 5_Streaming_Analytics/lab
export BASE_DIR=$(pwd)
```

2. Next, install the packages you will need to execute your pipeline:

```
python3 -m pip install -q --upgrade pip setuptools wheel
python3 -m pip install apache-beam[gcp]
```

3. Ensure that the Dataflow API is enabled:

`gcloud services enable dataflow.googleapis.com`

4. Finally, grant the dataflow.worker role to the Compute Engine default service account:

```
PROJECT_ID=$(gcloud config get-value project)
export PROJECT_NUMBER=$(gcloud projects list --filter="$PROJECT_ID" --format="value(PROJECT_NUMBER)")
export serviceAccount=""$PROJECT_NUMBER"-compute@developer.gserviceaccount.com"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:${serviceAccount}" --role="roles/dataflow.worker"
```

**Set up the data environment:**

```
# Create GCS buckets and BQ dataset
cd $BASE_DIR/../..
source create_streaming_sinks.sh
# Change to the directory containing the practice version of the code
cd $BASE_DIR
```

### **Task 1: Read from a streaming source**

In the previous labs, you used beam.io.ReadFromText to read from Google Cloud Storage. In this lab, instead of Google Cloud Storage, you use Pub/Sub. Pub/Sub is a fully managed real-time messaging service that allows publishers to send messages to a "topic," to which subscribers can subscribe via a "subscription."

![Ingest Stream](../static/images/df_ingest_stream_with_python_00.png "Ingest data stream python way")

The pipeline you create subscribes to a topic called `my_topic` that you just created via `create_streaming_sinks.sh` script. In a production situation, this topic will often be created by the publishing team.

### **Task 2: Sum page views per user:**

Open up `batch_user_traffic_pipeline.py` in your Cloud Shell Editor, which can be found in `3_Batch_Analytics/lab/`. This pipeline already contains the necessary code to accept command-line options for the input path and the output table name, as well as code to read in events from Google Cloud Storage, parse those events, and write results to BigQuery. However, some important parts are missing.

The next step in the pipeline is to aggregate the events by each unique `user_id` and count page views for each. An easy way to do this on objects of type `beam.Row` or objects with a Beam schema is to use the `GroupBy transform` and then perform some aggregations on the resulting group. For example:

`purchases | GroupBy('user_id', 'address')`

will return a PCollection of rows with two fields. The first is a `Row` with schema representing every unique combination of `user_id` and `address` (both strings), "key", and "values". The second field is an iterable of type `Row` containing all of the objects in the unique group from the first field.

This is most useful when you can perform aggregate calculations on this grouping and name the resulting fields, like so:

```
(purchases | GroupBy('user_id')
             .aggregate_field("item_id", CountCombineFn(), "num_purchases")
             .aggregate_field("cost_cents", sum, "total_spend_cents")
             .aggregate_field("cost_cents", max, "largest_purchases"))
             .with_output_types(UserPurchases)
             
```
This returns a `Row` with fields corresponding to the "key(s)" we grouped by and the corresponding aggregations computed here.

The `aggregate_field` method takes three arguments. The first argument is a string, referring to the name of the field we wish to aggregate in the input PCollection's schema. The second is the combiner we wish to apply, implemented as a subclass of [CombineFn](https://beam.apache.org/releases/pydoc/2.28.0/apache_beam.transforms.core.html#apache_beam.transforms.core.CombineFn). The third argument is a string that we use to identify the aggregation in the schema of the output PCollection.

Certain aggregation functions, such as `sum` and `max`, are implemented directly as combiners in Beam Python [(Link)](https://beam.apache.org/releases/pydoc/2.28.0/apache_beam.transforms.core.html#apache_beam.transforms.core.CombinePerKey). Count is implemented via [CountCombineFn](https://beam.apache.org/releases/pydoc/2.28.0/apache_beam.transforms.combiners.html#apache_beam.transforms.combiners.Count).

The output PCollection by default is a PCollection of type `Row`, but we can also apply our own custom types with schema using `with_output_types`. We see that above with `UserPurchases`. However, this means that we need to define a schema for type `UserPurchases`. We can do so easily by creating a subclass of `typing.NamedTuple` or via creating the schema ad hoc using beam.Row or `beam.Select`. We will cover the first case here. For the second please see the [Beam programming guide](https://beam.apache.org/documentation/programming-guide/#schema-definition).

The output of our aggregation above has four fields: `user_id` (type str), `num_purchases`, `total_spend_cents`, and `largest_purchases` (all type int). We create a subclass of NamedTuple with these field names and types then register the coder for the schema:

```
class UserPurchases(typing.NamedTuple):
  user_id : str
  num_purchases : int
  total_spend_cents : int
  largest_purchases : int
beam.coders.registry.register_coder(UserPurchases, beam.coders.RowCoder)
```

```
NOTE: In this example you could aggregate on any of the fields for `CountCombineFn()`, or even on the wildcard field `*`, as this transform is simply counting how many elements are in the entire group.
```

The next step in the pipeline is to aggregate events by `user_id`, sum the pageviews, and also calculate some additional aggregations on `num_bytes`, for example total user bytes, maximum user bytes, and minimum user bytes.

To complete this task, add another transform to the pipeline that groups the events by user_id and then performs the relevant aggregations. Keep in mind the input, the CombineFns to use, and how you name the output fields. After this, create a new output type with schema (call it PerUserAggregation) and ensure that the output Row is converted into this type. The tasks in the code are marked with # TODO.

### Task 3: Run your pipeline

```
export PROJECT_ID=$(gcloud config get-value project)
export REGION='us-central1'
export BUCKET=gs://${PROJECT_ID}
export PIPELINE_FOLDER=${BUCKET}
export RUNNER=DataflowRunner
export INPUT_PATH=${PIPELINE_FOLDER}/events.json
export TABLE_NAME=${PROJECT_ID}:logs.user_traffic
cd $BASE_DIR
python3 batch_user_traffic_pipeline.py \
--project=${PROJECT_ID} \
--region=${REGION} \
--staging_location=${PIPELINE_FOLDER}/staging \
--temp_location=${PIPELINE_FOLDER}/temp \
--runner=${RUNNER} \
--input_path=${INPUT_PATH} \
--table_name=${TABLE_NAME}
```


### Interesting links:

* [Code Repo](https://github.com/GoogleCloudPlatform/training-data-analyst/tree/master/quests/dataflow_python/3_Batch_Analytics "repo")
 * [Solution Code](https://github.com/GoogleCloudPlatform/training-data-analyst/blob/master/quests/dataflow_python/3_Batch_Analytics/solution/batch_user_traffic_pipeline.py "Solution")

