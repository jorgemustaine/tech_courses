# Dataproc: Qwik Start - Console

Cloud Dataproc is a fast, easy-to-use, fully-managed cloud service for running [Apache Spark](https://spark.apache.org/) and [Apache Hadoop](https://hadoop.apache.org/) clusters in a simpler, more cost-efficient way. Operations that used to take hours or days take seconds or minutes instead. Create Cloud Dataproc clusters quickly and resize them at any time, so you don't have to worry about your data pipelines outgrowing your clusters.

This lab shows you how to use the Google Cloud Console to create a Google Cloud Dataproc cluster, run a simple [Apache Spark](https://spark.apache.org/) job in the cluster, then modify the number of workers in the cluster.

