# Serverless Data Processing with Dataflow - Advanced Streaming Analytics Pipeline with Cloud Dataflow (Python)

## Lab part 1: Dealing with late data

In the previous labs, you wrote code that divided elements by event time into windows of fixed width, using code that looked like the following:


```
parsed_msgs
        | "WindowByMinute" >> beam.WindowInto(beam.window.FixedWindows(window_duration))
        | "CountPerMinute" >> beam.CombineGlobally(CountCombineFn()).without_defaults()
```

However, as you saw at the end of the last non-SQL lab, streams of data often have lag. Lag is problematic when windowing using event time (as opposed to processing time) because it introduces uncertainty: have all of the events for a particular point in event time actually arrived, or haven’t they?

Clearly, in order to output results, the pipeline you wrote needed to make a decision in this respect. It did so using a concept called a watermark. A watermark is the system’s heuristic-based notion of when all data up to a certain point in event time can be expected to have arrived in the pipeline. Once the watermark progresses past the end of a window, any further element that arrives with a timestamp in that window is considered late data and is simply dropped. So, the default windowing behavior is to emit a single, hopefully complete result when the system is confident that it has all of the data.

Apache Beam uses a number of heuristics to make an educated guess about what the watermark is. However, these are still heuristics. More to the point, those heuristics are general-purpose and are not suitable for all use cases. Instead of using general-purpose heuristics, pipeline designers need to thoughtfully consider the following questions in order to determine what tradeoffs are appropriate:

* Completeness: How important is it to have all of your data before you compute your result?
* Latency: How long do you want to wait for data? For example, do you wait until you think you have all data, or do you process data as it arrives?
* Cost: How much compute power and money are you willing to spend to lower the latency?

Armed with those answers, it’s possible to use Apache Beam’s formalisms to write code that makes the right tradeoff.

### Allowed lateness

Allowed lateness controls how long a window should retain its state; once the watermark reaches the end of the allowed lateness period, all state is dropped. While it would be great to be able to keep all of our persistent state around until the end of time, in reality, when dealing with an unbounded data source, it’s often not practical to keep a given window's state indefinitely; we’ll eventually run out of disk space.

As a result, any real-world, out-of-order processing system needs to provide some way to bound the lifetimes of the windows it’s processing. A clean and concise way of doing this is by defining a horizon on the allowed lateness within the system, i.e. placing a bound on how late any given record may be (relative to the watermark) for the system to bother processing it; any data that arrives after this horizon is simply dropped. Once you’ve bounded how late individual data may be, you’ve also established precisely how long the state for windows must be kept around: until the watermark exceeds the lateness horizon for the end of the window.

## Task 2: Set allowed lateness

1. In the file explorer, navigate to `training-data-analyst/quest/dataflow_python/7_Advanced_Streaming_Analytics/lab` and open the `streaming_minute_traffic_pipeline.py` file.

In Apache Beam, allowed lateness is set using the `allowed_lateness` keyword argument with the `AfterWatermark()` trigger within the `WindowInto` PTransform, as in the example below:

```
items = p | ...
Windowed_items = items | beam.WindowInto(beam.window.FixedWindows(60), # 1 minute
                                         trigger=AfterWatermark(),
                                         allowed_lateness=60*60*24) # 1 day
```

## Triggers
Pipeline designers also have discretion over when to emit preliminary results. In the previous step we used the `AfterWatermark()` trigger with a specified allowed lateness. For example, say that the watermark for the end of a window has not yet been reached, but 75% of the expected data has already arrived. In many cases, such a sample can be assumed to be representative, making it worth showing to end users.

Triggers determine at what point during processing time results will be materialized. Each specific output for a window is referred to as a pane of the window. Triggers fire panes when the trigger’s conditions are met. In Apache Beam, those conditions include `watermark progress`, `processing time progress` (which will progress uniformly, regardless of how much data has actually arrived), `element counts` (such as when a certain amount of new data arrives), and `data-dependent triggers`, like when the end of a file is reached.

A trigger’s conditions may lead it to fire a pane many times. Consequently, it’s also necessary to specify how to accumulate these results. Apache Beam currently supports two accumulation modes, one which accumulates results together and the other which returns only the portions of the result that are new since the last pane fired.

## Task 3: Set a trigger

When you set a windowing function for a `PCollection` by using the `Window` transform, you can also specify a trigger.

You set the trigger(s) for a PCollection by setting the `trigger` keyword argument of your `WindowInto` PTransform. Apache Beam comes with a number of provided triggers:

* [AfterWatermark](https://beam.apache.org/releases/pydoc/2.28.0/apache_beam.transforms.trigger.html#apache_beam.transforms.trigger.AfterWatermark) for firing when the watermark passes a timestamp determined from either the end of the window or the arrival of the first element in a pane.
* [AfterProcessingTime](https://beam.apache.org/releases/pydoc/2.28.0/apache_beam.transforms.trigger.html#apache_beam.transforms.trigger.AfterCount) for firing after some amount of processing time has elapsed (typically since the first element in a pane).
* [AfterCount](https://beam.apache.org/releases/pydoc/2.28.0/apache_beam.transforms.trigger.html#apache_beam.transforms.trigger.AfterCount) for firing when the number of elements in the window reaches a certain count.

This code sample sets a time-based trigger for a PCollection that emits results one minute after the first element in that window has been processed. In the last line of the code sample, we set the window’s accumulation mode by defining the keyword argument accumulation_mode to `AccumulationMode.DISCARDING`:

```
items = p | ...
windowed_items = items | beam.WindowInto(FixedWindows(60), # 1 minute
                                         trigger=AfterProcessingTime(60),
                                         accumulation_mode=AccumulationMode.DISCARDING)
```

## Lab part 2: Dealing with malformed data

Depending on how you set up your `Trigger`, if you were to run the pipeline right now and compare it to the pipeline from the previous lab, you might notice that the new pipeline presents results earlier. It’s also possible that its results might be more accurate, if the heuristics did a poor job of predicting streaming behavior and the allowed lateness is better.

However, while the current pipeline is more robust to lateness, it is still vulnerable to malformed data. If you were to run the pipeline and publish a message containing anything but a well-formed JSON string that could be parsed into a `CommonLog`, the pipeline would generate an error. Although tools like Cloud Logging make it straightforward to read those errors, a better-designed pipeline will store these in a pre-defined location for later inspection.

In this section, you add components to the pipeline that make it both more modular as well as more robust.

### Task 1: Collect malformed data

In order to be more robust to malformed data, the pipeline needs a way of filtering out this data and branching to process it differently. You have already seen one way to introduce a branch into a pipeline: by making one PCollection the input the multiple transforms.

This form of branching is powerful. However, there are some use cases where this strategy is inefficient. For example, say you want to create two different subsets of the same PCollection. Using the multiple transform method, you would create one filter transform for each subset and apply them both to the original PCollection. However, this would process each element twice.

An alternative method for producing a branching pipeline is to have a single transform produce multiple outputs while processing the input `PCollection` one time. In this task, you write a transform that produces multiple outputs, the first of which are the results obtained from well-formed data and second of which are the malformed elements from the original input stream.

In order to emit multiple results while still creating only a single `PCollection`, Apache Beam uses a class called TaggedOutput to key the outputs of the DoFn with multiple (possibly heterogeneous) outputs.

Here is an example of `TaggedOutput` being used to tag different outputs from a DoFn. Those PCollections are then recovered using the `with_outputs()` method and referenced with the tag name specified in the `TaggedOutput`.

```
class ConvertToCommonLogFn(beam.DoFn):
  def process(self, element):
    try:
        row = json.loads(element.decode('utf-8'))
        yield beam.pvalue.TaggedOutput('parsed_row', CommonLog(**row))
    except:
        yield beam.pvalue.TaggedOutput('unparsed_row', element.decode('utf-8'))
…
rows = (p | 'ReadFromPubSub' >> beam.io.ReadFromPubSub(input_topic)
              | 'ParseJson' >> beam.ParDo(ConvertToCommonLogFn()).with_outputs('parsed_row', 'unparsed_row')
                                                                 .with_output_types(CommonLog))
(rows.unparsed_row | …
(rows.parsed_row | …
```

### Interesting links:

* [lab7 solution](https://github.com/GoogleCloudPlatform/training-data-analyst/blob/master/quests/dataflow_python/7_Advanced_Streaming_Analytics/solution/streaming_minute_traffic_pipeline.py)