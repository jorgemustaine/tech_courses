# Recipe about export data from billing to a dataset bigquery

That info is taken from qwiklab from google lika a part of my personal training
on top of GCP [qwiklab 90 link HERE](https://google.qwiklabs.com/quests/90)

* In the left panel, click Billing export. The Billing export window opens with the
BigQuery Export tab selected by default.

* Click EDIT SETTINGS to show the export options.

* Set Projects to the project that contains your billing account.

* Set Billing export dataset to the BigQuery dataset where you want to host this data.

* Click SAVE.

This kicks off a job where your billing data is saved as a table in the selected BigQuery dataset.
If you open the BigQuery UI you will find the table populated with your billing data.
It's as easy as that!

Note that a billing export takes some time to populate (a few hours to a day).

The billing data for this lab has already been exported to a table in BigQuery. Following
BigQuery's project.dataset.table convention, the full path to the billing data is:

~~~
ctg-storage.bigquery_billing_export.gcp_billing_export_v1_01150A_B8F62B_47D999
~~~

To find which service types are most and least used, you must determine:

* What types of services do the 4 projects use.
* Which service types are most and least used.

~~~
SELECT service.description FROM `ctg-storage.bigquery_billing_export.gcp_billing_export_v1_01150A_B8F62B_47D999`
GROUP BY service.description
~~~

**Query to find which service types are most and least used**

~~~
SELECT service.description, COUNT(*) AS num FROM `ctg-storage.bigquery_billing_export.gcp_billing_export_v1_01150A_B8F62B_47D999`
GROUP BY service.description
~~~

**Finish with:** To the right of Query results, click Explore Data, and then Explore with Data Studio.

