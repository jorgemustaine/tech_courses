# Cloud Development training

[qwiklab quest: Cloud Development](https://googlecourses.qwiklabs.com/quests/67)

### Import and use the python Datastore module

in `...quiz/gcp/datastore.py` and import lib `os` to recovery env vars.

~~~
# TODO: Import the os module
import os
# END TODO
# TODO: Get the GCLOUD_PROJECT environment variable
project_id = os.getenv('GCLOUD_PROJECT')
# END TODO
from flask import current_app
# TODO: Import the datastore module from the google.cloud package
from google.cloud import datastore
# END TODO
# TODO: Create a Cloud Datastore client object
# The datastore client object requires the Project ID.
# Pass through the Project ID you looked up from the
# environment variable earlier
datastore_client = datastore.Client(project_id)
# END TODO
~~~

### Write code to create a Cloud Datastore entity.


* Use the Datastore client object to create a key for a Datastore entity whose kind is 'Question'.
* Use Datastore to create a Datastore question entity with the key.
* Iterate over the items in the dictionary of values supplied from the Web application form.
* In the body of the loop, assign each key and value to the Datastore entity object.
* Use the Datastore client to save the data.

~~~
"""
Create and persist and entity for each question
The Datastore key is the equivalent of a primary key in a relational database.
There are two main ways of writing a key:
1. Specify the kind, and let Datastore generate a unique numeric id
2. Specify the kind and a unique string id
"""
def save_question(question):
# TODO: Create a key for a Datastore entity
# whose kind is Question
    key = datastore_client.key('Question')
# END TODO
# TODO: Create a Datastore entity object using the key
    q_entity = datastore.Entity(key=key)
# END TODO
# TODO: Iterate over the form values supplied to the function
    for q_prop, q_val in question.items():
# END TODO
# TODO: Assign each key and value to the Datastore entity
        q_entity[q_prop] = q_val
# END TODO
# TODO: Save the entity
    datastore_client.put(q_entity)
# END TODO
~~~

### interesting links:

* [google code samples](https://github.com/GoogleCloudPlatform/training-data-analyst)
* [code sample for this qwiklab](https://github.com/GoogleCloudPlatform/training-data-analyst/tree/master/courses/developingapps/python/datastore)
* [Datastore API python documentation](https://googleapis.dev/python/datastore/latest/client.html)
* [prepare environment file bash](https://github.com/GoogleCloudPlatform/training-data-analyst/blob/master/courses/developingapps/python/cloudstorage/start/prepare_environment.sh)

@jorgemustaine 2021
