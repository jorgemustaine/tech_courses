from flask import Flask, jsonify, request, render_template

app = Flask(__name__)

stores = [
    {"name": "my Wonderful Store",
     "items": [
         {
             "name": "My item",
             "price": 15.99
         }
     ]
     }
]


@app.route('/')
def home():
    return render_template('index.html')


# POST - used to receive data
# GET - used to send data only


# POST /store data: {name:foo} ----->   create a store with name
@app.route('/store', methods=['POST'])
def create_store():
    """this method create a store"""
    request_data = request.get_json()
    new_store = {
        'name': request_data['name'],
        'itmes': []
    }
    stores.append(new_store)
    return jsonify(new_store)


# GET /store/<string:name>     ----->   obtain store with name
@app.route('/store/<string:name>') # 'http://127.0.0.1:5000/store/some_name'
def get_store(name):
    """recover a store iterate over stores
    if the store matches, return it
    if not matches return error message"""
    for store in stores:
        if store['name'] == name:
            return jsonify(store)
    return jsonify({'message': 'store not found'})


# GET /store                   ----->   obtain a list with all stores
@app.route('/store')
def get_stores():
    """recover a list of stores"""
    return jsonify({'stores': stores})


# POST /store/<string:name>/item {name:bar, price:2.0}
#                                --->   create a item in a specific store
@app.route('/store/<string:name>/item', methods=['POST'])
def create_item_in_store(name):
    """this method create an item in store"""
    request_data = request.json()
    for store in stores:
        if store['name'] == name:
            new_item = {
                'name': request_data['name'],
                'price': request_data['price']
            }
            store['items'].append(new_item)
            return jsonify(new_item)
    return jsonify({'message': 'store not found'})

# GET /store/<string:name>/item  {name:bar}
#                                --->   obtain item from a specific store
@app.route('/store/<string:name>/item', methods=['GET'])
def get_item_in_store(name):
    """this method create a store"""
    for store in stores:
        if store['name'] == name:
            return jsonify({'items':store['items']})
    return jsonify ({'message': 'store not found'})


app.run(port=5000)
