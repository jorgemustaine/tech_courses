# -*- coding: utf-8-*-
#  This python script is for coursera python poo course sponsored by inefop
#  by @jorgescalona @jorgemustaine 2020
#  use python 3.6 or higger

import unittest

products = {}
total_to_pay = 0


def add_products():
    open_pos = True
    while open_pos:
        option = input("Desea agregar un producto SI/NO: ")
        try:
            if option.isalpha():
                if option[0].lower() == "s":
                    product = input("ingrese el producto: ")
                    price = int(input("ingrese el precio: "))
                    products[product] = price
                elif option[0].lower() == "n":
                    open_pos = False
                else:
                    print("opción no valida")
            else:
                print("la opción no puede ser un número")
        except:
            open_pos = True
    print("Productos a pagar: ")
    for i in products:
        print(i, ": {0}".format(products[i]))


def buy():
    if len(products) > 0:
        open_pos = True
        total = 0
        while open_pos:
            for i in products:
                print(i, ": {0}".format(products[i]))
            product = input("Ingrese item o producto: ")
            for i in products:
                if i == product:
                    qty = input("Cantidad: ")
                    sub_total = int(qty) * products[i]
                    total = total + sub_total

                    print('producto {0}: cantidad {1}: Subtotal {2}.'.format(
                        i, qty, total))
                    other = True
                    while other:
                        another = input("agregar otro item SI/NO: ")
                        if another[0].lower() == "s":
                            open_pos = True
                            other = False
                        elif another[0].lower() == "n":
                            open_pos = False
                            other = False
                            return total
                        else:
                            print("Opción no válida")
                            other = True
    else:
        print("Productos no existen")


#   Factura!
def bill():
    open_pos = True
    while open_pos:
        print("1)Gold")
        print("2)Silver")
        print("3)Ninguna")
        discount = input(
            "Indique el tipo de total_discount con el número asociado: ")
        try:
            if not discount.isalpha():
                if discount == "1":
                    fact = 0.05
                    label = "5%"
                elif discount == "2":
                    fact = 0.02
                    label = "2%"
                else:
                    fact = 1
                    label = "Sin Descuento"

            print("El Descuento es {0}:".format(label))
            print("El subtotal de la factura es.{0}".format(total_to_pay))
            IVA = (total_to_pay * 0.12)
            total_discount = (total_to_pay * fact)
            total = total_to_pay + IVA - total_discount
            #  aqui comienza facturación
            print("Debe: {0}".format(total_to_pay))
            print("______________________")
            name = input("Nombre Del Cliente: ")
            vat = input("NIT: ")
            cash = input("Efectivo :  ")
            rest = int(cash) - total
            print("__________________________")
            print("Precio       {0}.2f\t".format(total_to_pay))
            print("IVA          {0}.2f\t".format(IVA))
            print("Total        {0}.2f\t".format(total))
            print("Efectivo     {0}.2f\t".format(cash))
            print("__________________________")
            print("rest:   {0}".format(rest))
            break

        except:
            opcion3 = True
    print("Gracias por su compra, Regrese Pronto.")


get_out = False
while not get_out:
    print("POS coursera Inefop")
    print("-------------------")
    print("1.) Agregar Items al carrito")
    print("2.) Comprar")
    print("3.) facturar")
    print("-------------------")
    opt_menu = input("Seleccione una opción: ")
    try:
        if not opt_menu.isalpha():
            if opt_menu == "1":
                add_products()
                opt_menu_int = input("Desea volver al menu SI/NO: ")
                if opt_menu_int[0].lower() == "s":
                    get_out = False
                else:
                    break
            elif opt_menu == "2":

                total_to_pay = buy()
                print(total_to_pay)
                opt_menu_int = input("Desea volver al menu SI/NO: ")
                if opt_menu_int[0].lower() == "s":
                    get_out = False
                else:
                    break
            elif opt_menu == "3":
                print(bill())
                opt_menu_int = input("Desea vover al menu SI/NO: ")
                if opt_menu_int[0].lower() == "s":
                    get_out = False
                else:
                    break
    except:
        print("Vuelva Pronto!")

class PosTestCase(self):

    def test_buy_products_00(self):
        products = {
            'avena': 25,
            'arroz': 5
        }
        buy(products)

if '__name__' == '__main__':
    unittest.main()