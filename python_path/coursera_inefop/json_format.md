# Json with python

Python is really very friendly when json data it's the task. The **json** lib convert
json to dict and reverse too.

import json

json.loads(obj)   · load string data in dict or list format

json.dumps(obj, sort_keys=True, indent=2)  ·  convert dict or list data in json
format

## interesting links:

* [json lib](https://docs.python.org/3.8/library/json.html)
