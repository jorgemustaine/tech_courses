#  Docker

**Pid 1:** The first process started by the linux kernel gets PID 1. Similarly
when new PID namespace is created first process started in that namespace gets
PID 1 (the PID as seen by the processes in that namespace, in the parent
namespace it gets assigned other PID).

The process with PID 1 differs from the other processes in the following ways:

   1. When the process with pid 1 die for any reason, all other processes are killed with KILL signal.
   1. When any process having children dies for any reason, its children are reparented to process with PID 1.
   1. Many signals which have default action of Term do not have one for PID 1.


### interesting commands:
**detener todos los contenedores:**
~~~
docker stop $(docker ps -aq)
~~~

## Docker engine

![docker engine](static/images/docker_engine_arch_00.png)

## docker workflow to build a image

![app deploy workflow](static/images/app_deploy_workflow.png)

### ENV ARG lifecycle

![app deploy workflow](static/images/env_arg_lifecycle_00.png)

### Logging

**systemd system:**
~~~
jounalctl -u docker.service
~~~

**no systemd system:**
~~~
/var/log/messages
~~~

![stdoutput stderr](static/images/logging_container_00.png)

### Docker Swarm

SwarmKit is a toolkit for orchestrating distributed systems at any scale. It
includes primitives for node discovery, raft-based consensus, task scheduling
and more.


* Secure cluster with CA encrypted algorithms
* Orchestation
* DNS through swarm.
* Load Balance through swarm.


**key commands:**

~~~
docker swarm init
docker swarm join
~~~

![docker components](static/images/docker_components_00.png)

## NetWorking with docker

![network docker](static/images/networking_with_docker_00.png)
![Networking Services Docker](static/images/networking_with_docker_01.png "Network Services")

## Docker Secrets

**Work with in docker swarm mode**

![docker secrets](static/images/docker_secrets_00.png "Docker Secrets")

## Docker Enterprise Tools

![docker EE tools](static/images/docker_enterprise_tools_00.png "Docker EE tools")

### Interesting links:

* [Pid 1](https://vagga.readthedocs.io/en/latest/pid1mode.html)
* [docker env var](https://vsupalov.com/docker-env-vars/)
* [env vars and secrets managemenmt](https://github.com/hms-dbmi/docker-images/wiki/Environment-Variable-and-Secrets-Management)
* [The Twelve Factor](https://12factor.net/)
* [Swarm](https://github.com/docker/swarmkit)
* [Docker Compose](https://docs.docker.com/compose/)
