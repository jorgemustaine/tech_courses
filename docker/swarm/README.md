# Swarm
![Swarm](../static/images/swarm_00.png)

It is a native tool of docker > 1.12 to manage cluster orchestation. Swarm contain
a set of tool that allow you to execute actions like:

1. Manage networking, IP, TCP, dns, ca certified and security.
1. Load balance.
1. Manage services.


## Swarm components:

Nodes:

* Manager (administer the cluster), a cluster can be have one or more managers.
stablish comunication with external clients by API way.
* Workers

**Filtering:** Swarm supprot three types of filter: affinity, Constraint, Resource.

![Swarm filter](../static/images/swarm_filter_00.png)

**Schedule:** Swarm support: random, spread (deploy workers between nodes a type of
LB), binpack (opposite of spread)

## Discovery Server HA high available disponibility

its a service that stored info about cluster like state and config via **libkv**

* consul
* etcd

## Swarm deploy plan

![Swarm deploy plan](../static/images/swarm_plan_00.png)
![Swarm expose port](../static/images/swarm_infrastructure_map_ports_00.png)


## Interesting links

* [swarm admin guide](https://docs.docker.com/engine/swarm/admin_guide/)
* [docker service](https://docs.docker.com/engine/reference/commandline/service/)
