#!/bin/bash

# Change filename extensions
# to generate examples files to test execute:
# touch bash/{a..h}.txt

if [[ $# -ne 2 ]]; then
    echo "Need exactly two arguments"
fi
echo *$1
for f in *"$1"; do
    base=$(basename "$f" "$1")
    # for the script doit your job change echo sentence and execute mv directily
    echo mv "$f" "${base}$2"
done

exit 0
