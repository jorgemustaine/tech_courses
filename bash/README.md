# BASH Scripting

![bash](static/images/bash_ico.png)

### Aritmethic operators

![aritmethic operators](static/images/aritmethic_test_operators_00.png)

##  Standards outputs & redirection

![standard outputs like files](static/images/redirection_stad_outputs_00.png)
![standard outputs table](static/images/redirection_stad_outputs_01.png)

## Interesting notes:

* for debug efect add **-x** in head of file:

~~~
#!/bin/bash -x
~~~

this dsiplay in screen line by line of execution

### interesting links:

* [bash env list and examples](https://www.tldp.org/LDP/abs/html/internalvariables.html)
