# Gitlab Learning


In this repo I'll collect all info about my learn process of gitlab tools.

![devops lifecicle](static/images/devops_lifecicle_on_gitlab_00.png "code lifecycle")

### devops lifecycle in gitlab:

![devops CICD lifecycle](static/images/gitlab_CICD_lifecicle_00.png "devops CICD lifecicle")


**How, can we activate the CICD cycle in gitlab?:**

To use GitLab CI/CD, all you need is an application codebase hosted in a Git
repository, and for your build, test, and deployment scripts to be specified in
a file called **.gitlab-ci.yml**, located in the root path of your repository.

An example for a gitlaci-yaml file:

![gitlac-ci yaml file sample](static/images/gitlab_ci_yaml_ex_00.png "sample yaml gitlab file")


### Interesting links:

* [Interactive gitlab platform](https://gitlab-core.us.gitlabdemo.cloud/)
* [gitlab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

@jorgemustaine 2021
