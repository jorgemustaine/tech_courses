# create nginx test lab with vagrant

### about logging in nginx

[nginx log documenetation](https://docs.nginx.com/nginx/admin-guide/monitoring/logging/)


## interesting links:

* [Provisioning Nginx using Vagrant](https://vegibit.com/how-to-provision-nginx-using-vagrant/)
* [Vagrant base template](https://github.com/RokasMuningis/vagrant-ubuntu-nginx)
* [Vagrant Intro](https://github.com/kevchn/hello-vagrant-nginx)

@jorgemustaine 2020
