variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}

variable "my_public_key" {
  type = string
}

variable "my_public_key_path" {
  type = string
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}

variable "region" {
  type    = string
  default = "us-west-1"
}

variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}

variable "AMIS" {
  type = map(any)
  default = {
    us-east-1 = "ami-04cc2b0ad9e30a9c8"
    us-west-2 = "ami-0a62a78cfedc09d76"
    us-west-1 = "ami-07be40433001d2433"
  }
}
