provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

resource "aws_key_pair" "jorge-key" {
  key_name   = "mykey"
  public_key = file(var.my_public_key_path)
}

resource "aws_instance" "example" {
  tags = {
    Name = "CourseExample"
  }
  ami           = lookup(var.AMIS, var.region)
  instance_type = "t2.micro"
  key_name      = aws_key_pair.jorge-key.key_name
  provisioner "file" {
    source      = "script.sh"
    destination = "/tmp/script.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo sed -i -e 's/\r$//' /tmp/script.sh", # Remove the spurious CR characters.
      "sudo /tmp/script.sh",
    ]
  }
  connection {
    host        = coalesce(self.public_ip, self.private_ip)
    type        = "ssh"
    user        = var.INSTANCE_USERNAME
    private_key = file(var.PATH_TO_PRIVATE_KEY)
  }
}

output "ip" {
  value = aws_instance.example.public_ip
}
