provider "aws" {
    profile = "default"
    region  = "us-east-1"
    #  region  = "us-east-1c"
}

resource "aws_instance" "example" {
    #  ami    = "ami-2757f631"
    ami    = "ami-026c8acd92718196b"
    instance_type = "t2.micro"
    ebs_block_device {
        device_name = "/dev/sdg"
        volume_size = 28
        volume_type = "gp2"
        #  delete_on_termination = false
    }
}

# Our default security group to access
# the instances over SSH and HTTP
#resource "aws_security_group" "default" {
#  name        = "terraform_example"
#  description = "Used in the terraform"
#  vpc_id      = "${aws_vpc.default.id}"
#
#  # SSH access from anywhere
#  ingress {
#    from_port   = 22
#    to_port     = 22
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  # HTTP access from the VPC
#  ingress {
#    from_port   = 80
#    to_port     = 80
#    protocol    = "tcp"
#    cidr_blocks = ["10.0.0.0/16"]
#  }
#
#  # outbound internet access
#  egress {
#    from_port   = 0
#    to_port     = 0
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#}

#resource "aws_key_pair" "auth" {
#  key_name   = "${var.key_name}"
#  public_key = "${file(var.public_key_path)}"
#}

