# kubectl commands:

With this command you can execute a shell inside the pod for debug his status.
### `kubectl exec -it name-of-the-pod -- sh`  

with this instruction you can see some pod metada.
### `kubectl get pod name-of-the-pod -o custom-columns=NAME:metadata.name,POD_IP:status.podIP` 

with this command you can see more info about the pod his IP for example.
### `kubectl get pod name-of-the-pod -o wide` 

with that you can display the std-output of the app inside the pod.
### `kubectl logs --tail=2 name-of-the-pod` 

idem above but with docker command.
### `docker container logs --tail=2 $(docker container ls -q --filter label=io.kubernetes.pod.name=name-of-the-pod)` 

for copy files into or from the pod
### `kubectl cp` 

for delete some element in the cluster.
### `kubectl delete -f original_file.yaml` 

very nice output for debug status of the cluster and their components from etcd.
### `kubectl get deployment name -o yaml > name-rsult.yaml` 

### interesting links:

* [lint for yaml file](http://www.yamllint.com/)
* [another lint for yaml file](https://codebeautify.org/yaml-validator)
* [nana commands](https://gitlab.com/nanuchi/youtube-tutorial-series/-/blob/master/basic-kubectl-commands/cli-commands.md)
