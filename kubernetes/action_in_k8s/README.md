# Kubernetes in action

[Kubernetes in action](https://www.manning.com/books/kubernetes-in-action-second-edition) It's a nice book from [Manning Publications](https://www.manning.com/)
by Marko Lukša teaches you to use Kubernetes to deploy container-based
distributed applications. You'll start with an overview of how Docker containers
work with Kubernetes and move quickly to building your first cluster.




@jorgemustaine 2023
