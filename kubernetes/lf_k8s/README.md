# k8s Linux Foundation 
### Linux FoundationX LFS158x
### Introduction to Kubernetes.


Documentation around Linux foundation course.


## Notes:

Minikube supports a `--drive=none` (on linux) that option runs k8s components
on the host OS and not inside a VM. The same form you can use `--driver=docker`
or `--driver=podman` for example for change the minikube runtime.

### Interesting links

* [k8s tools documentation](https://kubernetes.io/docs/tasks/tools/)
* [minikube docs](https://minikube.sigs.k8s.io/docs/)


@jorgemustaine 2021

