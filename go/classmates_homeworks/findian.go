package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	fmt.Println("Enter a string:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan() 
	input := scanner.Text()

	input = strings.ToLower(input)
	startsWithI := strings.HasPrefix(input, "i")
	endsWithN := strings.HasSuffix(input, "n")
	hasI := strings.Contains(input, "a")

	if startsWithI && endsWithN && hasI {
		fmt.Println("Found!")

	} else {
		fmt.Println("Not Found!")
	}
}


