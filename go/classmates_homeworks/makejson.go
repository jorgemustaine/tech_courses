package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
)

func main() {
	// read names and addresses from the console
	fmt.Printf("Enter in the name : ")
	reader := bufio.NewReader(os.Stdin)
	theName, _ := reader.ReadString('\n')

	fmt.Printf("Enter in the address : ")
	reader = bufio.NewReader(os.Stdin)
	theAddress, _ := reader.ReadString('\n')

	// create a map, keys "address", "name"
	addrBook := map[string]string{"name": theName, "addr": theAddress}
	addrBookJson, _ := json.Marshal(addrBook)

	// print out the json
	fmt.Println("The JSON representation is (and yeah it has the \\n or \\r\\n from the [enter]):\n")
	fmt.Println(string(addrBookJson))
}
