package main

import "fmt"

func main()  {
	var input float64
	fmt.Println("Enter a float number:")
	_, err := fmt.Scanf("%f", &input)
	if err==nil{
		fmt.Println(int64(input))
	}else{
		fmt.Println("Incorrect Input")
	}
}
