var x int = 1
var y int
var ip *int // ip is a pointer to int

ip = &x // ip now points to x
y = *ip // y is now 1
