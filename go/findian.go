package main
// writed by jorgescalona for go course on coursera
import (
    "fmt"
    "strings"
)

func main() {
    // declare var
    var word string
    // read the number from user
    fmt.Println("Add the word to sniff it: ")
    fmt.Scan(&word)
    len_word := len(word)
    if len_word < 3 {
        fmt.Println("Not Found!")
        return
    }
    word_l := strings.ToLower(word)
    if word_l[0] == 'i' && word_l[len(word_l)-1] == 'n' && strings.Contains(word_l, "a") {
        fmt.Println("Found!")
        return
    }
    fmt.Println("Not Found!")
}
